/* 
* From https://github.com/royale-proxy/node-cr-proxy/blob/master/lib/server/crypto.js
* No sense in reinventing the wheel, this already works perfectly
*/


var nacl = require("tweetnacl");
var blake2 = require("blake2");
var ByteBuffer = require("../../util/bytebuffer-sc");
var EMsg = require('../../enums/emsg');
const Crypto = require('../crypto.js');
const Nonce = require('../nonce.js');

class ServerCrypto extends Crypto {
    constructor(settings) {
        super();
        this.privateKey = new Buffer("1891d401fadb51d25d3a9174d472a9f691a45b974285d47729c45c6538070d85", "hex");
        this.serverKey = new Buffer("72f1a4a4c48e44da0c42310f800e96624e6dc6a641a9d41c3b5039d8dfadc27e", "hex");
    }

    setClient(client) {
        this.client = client;
    }

    decryptPacket(message) {
        if (message.messageType == EMsg.ClientHello ) {
            message.decrypted = message.payload;
        }  else {
            message.decrypted = this.decrypt(message.payload);
        }
    }

    encryptPacket(message) {
        if (message.messageType == EMsg.ServerHello || message.messageType == EMsg.LoginFailed) {
            message.encrypted = message.decrypted;
        }else {
            message.encrypted = this.encrypt(message.decrypted);
        }
    }
}

module.exports = ServerCrypto;
