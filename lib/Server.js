const net = require("net")
const settings = require("../settings.json")
const PacketReciever = require("./PacketReciever")
const Player = require("./Player")
const ServerCrypto = require("./server/crypto")
const ClientCrypto = require("./client/crypto")
const Definitions = require("./definitions")
const EMsg = require("../enums/emsg")
const jsome = require("jsome")
const handlers = require("../handlers")

const definitions = new Definitions.decode({})

module.exports = class Server {
    
    constructor() {

       this.tcp = net.createServer()
       this.ready = false
       this.connections = {}

        this.tcp.listen({ host: "192.168.178.35", port: 9339, exclusive: true }, (err) => {
            if (err) console.error(err)
        })

        this.tcp.on("connection", (socket) => {

            socket.key = `${ socket.remoteAddress }:${ socket.remotePort }`

            this.connections[socket.key] = socket

            const packet_reciever = new PacketReciever()

            const crypto = new ServerCrypto(settings)
            const client_crypto = new ClientCrypto(settings)

            

            crypto.setClient(client_crypto)
            client_crypto.setServer(crypto)

            socket.on("data", (chunk) => {

                console.log("")
                console.log("data ok ")
                console.log("")

                packet_reciever.packetize(chunk, (packet) => {

                    const message = {
                        messageType: packet.readUInt16BE(0),
                        length: packet.readUInt16BE(2, 3),
                        version: packet.readUInt16BE(5),
                        payload: packet.slice(7, packet.length)
                    }

                    const id = EMsg[message.messageType] ? EMsg[message.messageType] : message.messageType
                    const id1 = EMsg[message.payload] ? EMsg[message.payload] : message.payload
                    //console.log(`[RICEVUTO] ${ id } (/) ${ id1 }`)

                    
                    console.log(`[GOT] ${ id } `)

                    crypto.decryptPacket(message)
                    definitions.decode(message)

                    if(message.decoded && Object.keys(message.decoded).length) {
                        jsome(message.decoded)
                    }

                    if(handlers[id]) {
                        handlers[id](message, crypto, socket)
                    } else {
                        console.log(`[ERRORE] No packet handler exists for type: ${ id }`)
                    }

                })
            })
        })

        this.tcp.on("listening", () => {
            this.ready = true
            console.log("Server online")
        })

        this.tcp.on("error", (err) => {
            if(err) {
                console.error(err)
            } else {
                console.log("[ERR] error")
            }
        })
    }
}