const EMsg = require("../enums/emsg")
const Long = require("long")
const ByteBuffer = require("../util/bytebuffer-sc")
const fs = require("fs")
const defs = require("../lib/definitions")
const defencode = new defs.encode()

module.exports = (message, crypto, socket) => {

    let decrypted = {
         userId:0,
        homeId: 0,
        userToken: "h3gbtfjyk8p7gerwscak3je9a363wymb7c47fzes",
        gameCenterId: "",
        facebookId: "",
        serverMajorVersion: 3,
        serverBuild: 377,
        contentVersion: 2,
        environment: "stage",
        sessionCount: 0,
        playTimeSeconds: 0,
        daysSinceStartedPlaying: 0,
        facebookAppId: "1609113955765603",
        serverTime: "",
        accountCreatedDate: "0",
        unknown_16: 0,
        googleServiceId: "",
        unknown_18: "",
        unknown_19: "",
        region: "IT",
        contentURL: "Spinea",
        eventAssetsURL: "0",
        unknown_23: 1,

    }
    
    let msg = {
        messageType: EMsg.LoginOk,
        decoded: decrypted
    }


    defencode.encode(msg)
    crypto.encryptPacket(msg)

    var header = Buffer.alloc(7)
    header.writeUInt16BE(EMsg.LoginOk, 0)
    header.writeUInt16BE(msg.encrypted.byteLength, 2, 3) 
    header.writeUInt16BE(message.version, 5) 


    socket.write(Buffer.concat([header, Buffer.from(msg.encrypted)]))
    console.log("sent LoginOk")
}



/*let decrypted1 = {
        unknown_1: "",
        unknown_2: "",
        unknown_3: 0,
        unknown_4: "",
        unknown_5: "",
        unknown_6: "",
    }
    let msg1 = {
        messageType: EMsg.OwnHomeData,
        decoded: decrypted1
    }

    socket.write(Buffer.concat([header, Buffer.from(msg1.encrypted)]))
    
*/